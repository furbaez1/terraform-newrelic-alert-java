variable "account_id" {
  type        = number
  description = "(Required) NewRelic Account to be used."
  default     = 3538507
}

variable "api_key" {
  type        = string
  description = "(Required) API from the account to be used."
  default     = "NRAK-Q5AYX8GEFPPJUK41JYXFNH7VHVQ"
}
