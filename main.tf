resource "newrelic_alert_policy" "foo" {
  name = "Java_alets"
  incident_preference = "PER_POLICY" # PER_POLICY is default
}

resource "newrelic_nrql_alert_condition" "transaction_errors" {
  policy_id = newrelic_alert_policy.foo.id
  type = "static"
  name = "Transaction Errors"

  description = <<-EOT
  This alert is triggered when the transaction error count is higher than 5 for 5 minutes.
  EOT

  enabled = true
  violation_time_limit_seconds = 86400

  nrql {
    query = "SELECT count(*) FROM TransactionError WHERE `error.expected` IS FALSE OR `error.expected` IS NULL"
  }

  critical {
    operator = "above"
    threshold = 5
    threshold_duration = 300
    threshold_occurrences = "all"
  }

  warning {
    operator = "above"
    threshold = 3
    threshold_duration = 300
    threshold_occurrences = "all"
  }
  fill_option = "none"
  aggregation_window = 60
  aggregation_method = "event_flow"
  aggregation_delay = 120
}

resource "newrelic_nrql_alert_condition" "average_transaction_duration" {
  policy_id = newrelic_alert_policy.foo.id
  type = "static"
  name = "Average Transaction Duration"

  description = <<-EOT
  This alert is triggered when Average Transaction Duration is higher than 2 seconds for 5 minutes.
  EOT

  enabled = true
  violation_time_limit_seconds = 86400

  nrql {
    query = "FROM Transaction SELECT average(duration) as 'Average Transaction Duration'"
  }

  critical {
    operator = "above"
    threshold = 2
    threshold_duration = 300
    threshold_occurrences = "all"
  }

  warning {
    operator = "above"
    threshold = 1
    threshold_duration = 300
    threshold_occurrences = "all"
  }
  fill_option = "none"
  aggregation_window = 60
  aggregation_method = "event_flow"
  aggregation_delay = 120
}

resource "newrelic_nrql_alert_condition" "high_cpu_utilization" {
  policy_id = newrelic_alert_policy.foo.id
  type = "static"
  name = "High CPU Utilization"

  description = <<-EOT
  This alert is triggered when the CPU Utilization is above 90%.
  EOT

  enabled = true
  violation_time_limit_seconds = 2592000

  nrql {
    query = "SELECT rate(sum(apm.service.cpu.usertime.utilization), 1 second) * 100 as cpuUsage FROM Metric WHERE appName like '%'"
  }

  critical {
    operator = "above"
    threshold = 90
    threshold_duration = 300
    threshold_occurrences = "all"
  }
  fill_option = "none"
  aggregation_window = 60
  aggregation_method = "event_flow"
  aggregation_delay = 120
}

resource "newrelic_nrql_alert_condition" "average_heap_memory_used" {
  policy_id = newrelic_alert_policy.foo.id
  type = "static"
  name = "Average Heap Memory Used"

  description = <<-EOT
  This alert is triggered when Average Heap Memory Used is higher than 2048 MB for 5 minutes.
  EOT

  enabled = true
  violation_time_limit_seconds = 86400

  nrql {
    query = "SELECT average(apm.service.memory.heap.used) as 'Used Heap' FROM Metric WHERE appName LIKE '%'"
  }

  critical {
    operator = "above"
    threshold = 2048
    threshold_duration = 300
    threshold_occurrences = "all"
  }

  warning {
    operator = "above"
    threshold = 1024
    threshold_duration = 180
    threshold_occurrences = "all"
  }
  fill_option = "none"
  aggregation_window = 60
  aggregation_method = "event_flow"
  aggregation_delay = 120
}

resource "newrelic_notification_destination" "webhook-destination" {
  name = "destination-webhook"
  type = "WEBHOOK"

  property {
    key = "url"
    value = "https://webhook.mywebhook.com"
  }
}

resource "newrelic_notification_channel" "webhook-channel" {
  name = "channel-webhook"
  type = "WEBHOOK"
  destination_id = newrelic_notification_destination.webhook-destination.id
  product = "IINT"

  property {
    key = "payload"
    value = "{name: foo}"
    label = "Payload Template"
  }
}

// workflow that matches issues that include incidents triggered by the policy
resource "newrelic_workflow" "workflow-example" {
  name = "workflow-example"
  muting_rules_handling = "NOTIFY_ALL_ISSUES"

  issues_filter {
    name = "Filter-name"
    type = "FILTER"

    predicate {
      attribute = "labels.policyIds"
      operator = "EXACTLY_MATCHES"
      values = [ newrelic_alert_policy.foo.id ]
    }
  }

  destination {
    channel_id = newrelic_notification_channel.webhook-channel.id
  }
}
